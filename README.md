# Mikrozeum setup
Website template with SCSS (Sass) & Gulp. It also includes Autoprefixer, Javascript files minification and Sourcemaps for CSS.

1. Run: git clone https://gitlab.com/Grilec/mikrozeum.git
1. Install dependencies: npm install
1. In root directory of website run the "gulp" command to start "watch" task for changes tracking
