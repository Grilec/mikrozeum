var tooltips = document.querySelectorAll('.tooltip span');

window.onmousemove = function (e) {
    var x = (e.clientX - 80) + 'px',
        y = (e.clientY + 25) + 'px';
    for (var i = 0; i < tooltips.length; i++) {
        tooltips[i].style.top = y;
        tooltips[i].style.left = x;
    }
};




$('.hamburger').on('click', function(){
    $('header').toggleClass('hamburger-opened');
    $('.hamburger-menu').toggleClass('active');
})