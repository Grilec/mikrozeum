var gulp = require('gulp');
var concat = require('gulp-concat');
var minify = require('gulp-minify');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');

var styleAssets = './assets/scss/style.scss';
var stylePublic = './public/build/css/';


gulp.task('script', function(){
    return gulp.src(['./assets/js/*.js'])
            .pipe(concat('script.js'))
            .pipe(minify({
                ext: {
                    min: '.js'
                },
                noSource: true
            }))
            .pipe(gulp.dest('public/build/js'));
});


gulp.task('style', function(){
    return gulp.src(styleAssets)
        .pipe(sourcemaps.init())
        .pipe(sass({
            errorLogToConsole: true,
            outputStyle: 'compressed'
        }))
        .on('error', console.error.bind(console))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false 
        }))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(stylePublic));
});


gulp.task('watch', function(){
    gulp.watch('./assets/js/**/*.js', gulp.series('script'));
    gulp.watch('./assets/scss/**/*.scss', gulp.series('style'));
});


gulp.task('default', gulp.series('watch'));